# gitlab-multi-runner

1. first to start gitlab-runner. [docker reference](https://docs.gitlab.com/runner/install/docker.html)

2. [prepare to register](https://docs.gitlab.com/runner/register/index.html) 

3. prepare a runner image for docker. 
  - Dockerfile
  - entrypoint.sh
  - docker-compose.yml